# -*- coding: utf-8  -*-

# A lire si vous faîtes une mise à jour et si vous avez ajouté ou modifié les commandes du bot :
# 1) Copiez vos commandes (pas les commandes par défaut) que vous avez créer dans votre ancienne version dans la nouvelle version.
# 2) Si vous avez modifié une commande de NextBot par défaut, supprimez la commande de la nouvelle version puis copiez le code de la commande de l'ancienne version dans la nouvelle version.

import asyncio, discord, datetime, random
# lib perso
import parameters
import includes

user_bot = parameters.user_bot
token = parameters.token
trust = parameters.trust
ranks = parameters.ranks
bot_mentions = ("<@!{0}>".format(parameters.client_id), "<@{0}>".format(parameters.client_id))

client = discord.Client()
ver = "1.0.0"
lang = "en"

print("I'm a seagull -- MINE")

@client.event
@asyncio.coroutine

def on_message(message):
    rep = text = msg = message.content
    rep2 = text2 = msg2 = rep.split()
    user = str(message.author)
    trusted = user in trust

    try:
        server_msg = str(message.channel.server)
        chan_msg = str(message.channel.name)
        pm = False
    except AttributeError:
        server_msg = user
        chan_msg = user
        pm = True
    try:
        command = rep2[0].lower()
        params = rep2[0:]
    except IndexError:
        command = ""
        params = ""

    if ranks and not pm:
        open("msgs_user_" + server_msg + ".txt", "a").close()
        msgs = open("msgs_user_" + server_msg + ".txt", "r")
        msgs_r = msgs.read()
        if user not in msgs_r:
            msgs_w = open("msgs_user_" + server_msg + ".txt", "a")
            msgs_w.write(user + ":0\n")
            msgs_w.close()
            msgs.close()
            msgs = open("msgs_user_" + server_msg + ".txt", "r")
            msgs_r = msgs.read()
        msgs_user = msgs_r.split(user + ":")[1]
        msgs.close()
        user_msgs_n = int(msgs_user.split("\n")[0])
        user_msgs_n += 1
        msgs_r = msgs_r.replace(user + ":" + str(user_msgs_n - 1), user + ":" + str(user_msgs_n))
        msgs = open("msgs_user_" + server_msg + ".txt", "w")
        msgs.write(msgs_r)
        msgs.close()

    # -----* Début des commandes *-----

    #Commandes simples
    content_message = message.content.split(" ")
    command = content_message.pop(0)
    server_id = message.server.id

    if command in bot_mentions:
        order = ""

        if len(content_message) > 0:
            order = content_message.pop(0)

        if order == "how_fucked_am_i":
            random_num = random.randint(0,2)

            if random_num == 0:
                yield from client.send_message(message.channel, "Very")
            elif random_num == 1:
                yield from client.send_message(message.channel, "Eh")
            elif random_num == 2:
                yield from client.send_message(message.channel, "Welp.")

        if order == "commands_list" or not order:
            yield from client.send_message(message.channel, "**List of available commands:**\n\n+ *help:* get help with a command's syntax\n+ *create_war:* put an end to the former word war and start a new one\n+ *war_update:* set your new total word count\n+ *war_add:* add to your current word count\n+ *team_wc:* get the detailed word count of all the participating teams and their members\n+ *ww_ranking:* get the current war ranking by participants' word counts\n+ *server_ranking:* get the current all time server ranking by participants' word counts\n+ *my_wc:* get your global word count from all the word wars you participated in\n\nIf you need specific help with a command, mention the bot and ask *help [command]*")

        if order == "help":
            command_to_explain = content_message.pop(0)

            if command_to_explain == 'create_war':
                yield from client.send_message(message.channel, "**Syntax for *create_war* is as follows:**\ncreate_war [with/without] teams [exact name of teams separated by commas]")

            if command_to_explain == 'war_update':
                yield from client.send_message(message.channel, "**Syntax for *war_update* is as follows:**\nwar_update [your word count (as a number, of course)]")

            if command_to_explain == 'war_add':
                yield from client.send_message(message.channel, "**Syntax for *war_add* is as follows:**\nwar_update [the number of words to add]")

        if order == "hi":
            user_mention = message.author.mention
            yield from client.send_message(message.channel, "Hi " + user_mention + "!")

        if order == "mine":
            yield from client.send_message(message.channel, "MINE MINE MINE MINE")

        # Créer une WW
        if order == "create_war":
            if content_message:
                # Avec ou sans équipes ?
                with_or_without = content_message.pop(0)
                with_teams = (with_or_without == "with")

                # On efface le "teams" qui doit venir après "with" ou "without"
                # et qui ne sert qu'au confort de l'utilisateur
                del content_message[0]

                # Il ne reste que les noms d'équipes ou rien du tout
                teams = " ".join(content_message)
                teams = teams.split(',')
                teams = [x.strip() for x in teams]
            else:
                with_teams = False
                teams = []

            result_new_ww = includes.create_war(server_id, with_teams, teams, message.server.roles)

            if result_new_ww != "error_creating_ww":
                yield from client.send_message(message.channel, "Word war was successfully created!")
            else:
                yield from client.send_message(message.channel, "Error while creating the War. Poke the admin!")

        # Set ou ajout à son WC de WW
        if order == "war_update" or order == "war_add":
            # Récupération du WC
            new_wc = content_message.pop(0)

            # Récupération du user concerné
            author_mention = message.author.mention
            author_id = str(message.author.id)

            # On s'assure que le WC soumis est bien un chiffre
            if(new_wc.isdigit()):
                roles_id = []

                for role in message.author.roles:
                    if not role.is_everyone:
                        roles_id.append(str(role.id))

                result = includes.update_wc(server_id, author_id, roles_id, new_wc, (order == "war_add"))

                # On vérifie que tout se soit bien passé
                if result == "ok":
                    # On confirme en mentionnant l'user que son WC est bien enregistré
                    if order == "war_update":
                        out_message = "New word count for {0} : {1}".format(author_mention,str(new_wc))
                    else:
                        out_message = "Added {0} words to {1}'s word count".format(str(new_wc), author_mention)
                else:
                    if result == "error_no_ww":
                        out_message = "Error: no WW currently in progress"

                yield from client.send_message(message.channel, out_message)

            # Donne un chiffre connard
            else:
                yield from client.send_message(message.channel, "I need a number!")

        # Affichage des WC de la WC
        if order == 'team_wc':
            teams_wc = includes.get_teams_wc(server_id)
            display_wc = ''

            if teams_wc:
                now = str(datetime.datetime.now()).split(' ')[0]
                display_wc = "**{0}:**\n".format(now)

                for team_id, members_wc in teams_wc.items():
                    display_wc+= "\n**Word count for <@&{0}>: {1}**\n".format(team_id, members_wc['total'])

                    for team_member, word_count in members_wc['team_members'].items():
                        display_wc+= "+ word count for <@{0}> : {1}\n".format(team_member, word_count)
            else:
                display_wc = 'No word count to display yet!'

            yield from client.send_message(message.channel, display_wc)

        if order == 'ww_ranking' or order == 'server_ranking':
            if order == 'ww_ranking':
                ranking = includes.get_ww_ranking(server_id)
                ranking_type = 'war'
            else:
                ranking = includes.get_server_ranking(server_id)
                ranking_type = 'server'

            if ranking:
                now = str(datetime.datetime.now()).split(' ')[0]
                display_wc = "**{0} - Current {1} ranking:**\n\n".format(now, ranking_type)
                i = 1

                for row in ranking:
                    display_wc+= "{0}. <@{1}> : {2}\n".format(i, row['id_member'], row['word_count'])
                    i+= 1
            else:
                display_wc = 'No word count to display yet!'

            yield from client.send_message(message.channel, display_wc)

        # TODO: que faire si la WW n'est pas en équipes ?
        if order == "create_battle":
            # Récupération des params de la battle
            battle_name = content_message.pop(0)
            start = content_message.pop(0) + " " + content_message.pop(0)
            end = content_message.pop(0) + " " + content_message.pop(0)

            result_new_battle = includes.create_battle(server_id, battle_name, start, end)
            yield from client.send_message(message.channel, "Success! {0}".format(result_new_battle))

        # Set ou ajout à son WC de WW
        if order == "battle_update" or order == "battle_add":
            # Récupération du WC
            new_wc = content_message.pop(0)

            # Récupération du user concerné
            author_mention = message.author.mention
            author_id = str(message.author.id)

            # On s'assure que le WC soumis est bien un chiffre
            if(new_wc.isdigit()):
                roles_id = []

                for role in message.author.roles:
                    if not role.is_everyone:
                        roles_id.append(str(role.id))

                result = includes.update_battle_wc(server_id, author_id, roles_id, new_wc, (order == "battle_add"))

                # On vérifie que tout se soit bien passé
                if result == "ok":
                    # On confirme en mentionnant l'user que son WC est bien enregistré
                    if order == "battle_update":
                        out_message = "New word count for {0} : {1}".format(author_mention,str(new_wc))
                    else:
                        out_message = "Added {0} words to {1}'s word count".format(str(new_wc), author_mention)
                else:
                    if result == "error_no_battle":
                        out_message = "Error: no battle currently in progress"

                yield from client.send_message(message.channel, out_message)

            # Donne un chiffre connard
            else:
                yield from client.send_message(message.channel, "I need a number!")

        # if order == "battle_time_remaining":

        if order == 'my_wc':
            author_id = message.author.id
            author_mention = message.author.mention
            server_id = message.server.id
            my_wc = includes.get_my_wc(server_id, author_id)
            yield from client.send_message(message.channel, "Word count for {0}: {1}".format(author_mention, my_wc['total']))

#Fin des commandes

client.run(token)

