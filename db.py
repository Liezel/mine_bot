import pymysql.cursors
import parameters

def connect_db():
    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user=parameters.db_user,
                                 password=parameters.db_password,
                                 db=parameters.db_base,
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection

def add_war(server_id, with_teams):
    connection = connect_db()

    try:
        with connection.cursor() as cursor:
            sql_close = "UPDATE `word_wars` SET `status`= 0 WHERE `id_server` = {0}".format(server_id);
            cursor.execute(sql_close)
            sql_add = "INSERT INTO `word_wars` (`id_server`, `with_teams`, `status`) VALUES ({0}, {1}, 1)".format(server_id, with_teams)
            cursor.execute(sql_add)

            connection.commit()
            id_ww = cursor.lastrowid
    finally:
        connection.close()

    return id_ww

def create_battle(server_id, battle_name, start, end):
    connection = connect_db()

    try:
        with connection.cursor() as cursor:
            sql_search = "SELECT `id` FROM `word_wars` WHERE `status` = 1 AND `id_server` = {0}".format(server_id)
            cursor.execute(sql_search)
            result_search = cursor.fetchone()

            if result_search['id']:
                id_ww = result_search['id']
                sql_add = "INSERT INTO `word_battles` (`id_ww`, `name`, `start`, `end`) VALUES ({0}, '{1}', '{2}', '{3}')".format(id_ww, battle_name, start, end)
                print(sql_add)
                cursor.execute(sql_add)
                connection.commit()
                id_battle = cursor.lastrowid
            else:
                return "error_no_ww"
    finally:
        connection.close()

    return id_battle

def add_teams_to_ww(id_ww, ww_teams):
    connection = connect_db()

    try:
        with connection.cursor() as cursor:
            in_p = ', '.join(['{'+str(i)+'}' for i, value in enumerate(ww_teams)])
            sql = "INSERT INTO `ww_teams` (`id_ww`, `id_discord`) VALUES {0};".format(in_p)
            sql = sql.format(*ww_teams)
            cursor.execute(sql)
            connection.commit()
    finally:
        connection.close()

def update_personnal_ww_wc(server_id, author_id, teams_id, new_wc, add, wc_type):
    connection = connect_db()

    try:
        with connection.cursor() as cursor:
            # TODO : d'abord vérifier si une WW est active, ENSUITE voir si on update ou si on insert

            teams_str = ','.join(teams_id)
            # On regarde si le user a déjà un WC à update pour cette WW et cette team
            sql = "SELECT tm.`id` AS id_ww, w.`with_teams` FROM `ww_word_counts` tm JOIN `word_wars` w ON w.`id` = tm.`id_ww` WHERE w.`id_server` = {0} AND w.`status` = 1 AND tm.`id_member` = {1} AND (tm.`id_team` IN ({2}) OR w.`with_teams` = 0) AND tm.`word_count_type` = '{3}';".format(server_id, author_id, teams_str, wc_type)
            cursor.execute(sql)
            result = cursor.fetchone()

            if result:
                if result['with_teams'] == 1 and not teams_id:
                    return "error_no_team"

                # On regarde si c'est un ajout ou un écrasement de valeur
                if add:
                    sql = "UPDATE `ww_word_counts` SET `word_count` = `word_count` + {0} WHERE `id` = {1} AND `word_count_type` = '{2}';".format(new_wc, result['id_ww'], wc_type)
                else:
                    sql = "UPDATE `ww_word_counts` SET `word_count` = {0} WHERE `id` = {1} AND `word_count_type` = '{2}';".format(new_wc, result['id_ww'], wc_type)

                cursor.execute(sql)
                connection.commit()
            else:
                # On cherche une WW active et les infos dessus (en équipe ou non, si oui quelles équipes)
                sql_ww = "SELECT ww.`id` AS id_ww, ww.`with_teams`, wt.`id_discord` AS id_team FROM `word_wars` ww LEFT JOIN `ww_teams` wt ON wt.`id_ww` = ww.`id` WHERE ww.`id_server` = {0} AND ww.`status` = 1 AND (wt.`id_discord` IN ({1}) OR ww.`with_teams` = 0);".format(server_id, teams_str)
                cursor.execute(sql_ww)
                result_ww = cursor.fetchone()

                # Si aucun résultat, on sait qu'il n'y a aucune WW en cours
                # ou que le type n'appartient pas à la bonne équipe
                if not result_ww:
                    return "error_no_ww"

                # TODO : c'est probablement censé check si le type appartient à la bonne équipe mais id_ww
                # a rien à foutre là dans ce cas
                # if result_ww['id_ww'] == 0 and not result_ww['id_team']:
                #     return "error_no_team"

                # Tout va bien : le type a le droit d'inscrire son score
                sql_insert = "INSERT INTO `ww_word_counts` (`id_ww`, `id_member`, `id_team`, `word_count_type`, `word_count`) VALUES ({0}, {1}, {2}, 'word_war', {3})".format(result_ww['id_ww'], author_id, result_ww['id_team'], new_wc)
                cursor.execute(sql_insert)
                connection.commit()

    finally:
        connection.close()

    return "ok"

def get_teams_wc(server_id):
    connection = connect_db()

    try:
        with connection.cursor() as cursor:
            sql = "SELECT `word_count`, t.`id_member`, t.`id_team` FROM `ww_word_counts` t JOIN `word_wars` w ON w.id = t.`id_ww` WHERE w.`id_server`= {0} AND w.`status` = 1 ORDER BY t.`id_team`".format(server_id)
            cursor.execute(sql)
            result = cursor.fetchall()
    finally:
        connection.close()

    return result

def get_ww_ranking(server_id):
    connection = connect_db()

    try:
        with connection.cursor() as cursor:
            sql = "SELECT t.`word_count`, t.`id_member` FROM `ww_word_counts` t JOIN `word_wars` w ON w.id = t.`id_ww` WHERE w.`id_server`= {0} AND w.`status` = 1 ORDER BY t.`word_count` DESC".format(server_id)
            cursor.execute(sql)
            result = cursor.fetchall()
    finally:
        connection.close()

    return result

def get_server_ranking(server_id):
    connection = connect_db()

    try:
        with connection.cursor() as cursor:
            sql = "SELECT SUM(t.`word_count`) AS word_count, t.`id_member` FROM `ww_word_counts` t JOIN `word_wars` w ON w.id = t.`id_ww` WHERE w.`id_server`= {0} GROUP BY t.`id_member` ORDER BY word_count DESC".format(server_id)
            cursor.execute(sql)
            result = cursor.fetchall()
    finally:
        connection.close()

    return result

def get_my_wc(server_id, author_id):
    connection = connect_db()

    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT SUM(`word_count`) AS total FROM `ww_word_counts` t JOIN `word_wars` w ON w.`id` = t.`id_ww` WHERE w.`id_server`= {0} AND t.`id_member` = {1} ORDER BY w.`id` DESC".format(server_id, author_id)
            cursor.execute(sql)

            result = cursor.fetchone()
    finally:
        connection.close()

    return result
