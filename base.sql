-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Sam 19 Mai 2018 à 19:50
-- Version du serveur :  5.7.22-0ubuntu18.04.1
-- Version de PHP :  7.2.5-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mine_bot`
--

-- --------------------------------------------------------

--
-- Structure de la table `team_members`
--

CREATE TABLE `team_members` (
	  `id` int(11) NOT NULL,
	  `id_member` varchar(255) NOT NULL,
	  `id_team` varchar(255) NOT NULL,
	  `id_ww` int(11) NOT NULL,
	  `word_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `word_battles`
--

CREATE TABLE `word_battles` (
	  `id` int(11) NOT NULL,
	  `id_ww` int(11) NOT NULL,
	  `name` text NOT NULL,
	  `start` datetime NOT NULL,
	  `end` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `word_wars`
--

CREATE TABLE `word_wars` (
	  `id` int(11) NOT NULL,
	  `id_server` varchar(255) NOT NULL,
	  `with_teams` tinyint(1) NOT NULL,
	  `status` tinyint(1) NOT NULL,
	  `start_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ww_teams`
--

CREATE TABLE `ww_teams` (
	  `id` int(11) NOT NULL,
	  `id_ww` int(11) NOT NULL,
	  `id_discord` varchar(255) NOT NULL,
	  `name` varchar(255) DEFAULT NULL,
	  `mention` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ww_word_counts`
--

CREATE TABLE `ww_word_counts` (
	  `id` int(11) NOT NULL,
	  `id_member` varchar(255) NOT NULL,
	  `id_team` varchar(255) NOT NULL,
	  `id_ww` int(11) NOT NULL,
	  `word_count_type` varchar(15) NOT NULL,
	  `id_battle` int(11) DEFAULT NULL,
	  `word_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `word_battles`
--
ALTER TABLE `word_battles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_ww` (`id_ww`);

--
-- Index pour la table `word_wars`
--
ALTER TABLE `word_wars`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ww_teams`
--
ALTER TABLE `ww_teams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_ww` (`id_ww`);

--
-- Index pour la table `ww_word_counts`
--
ALTER TABLE `ww_word_counts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_ww` (`id_ww`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `word_battles`
--
ALTER TABLE `word_battles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `word_wars`
--
ALTER TABLE `word_wars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT pour la table `ww_teams`
--
ALTER TABLE `ww_teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pour la table `ww_word_counts`
--
ALTER TABLE `ww_word_counts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `word_battles`
--
ALTER TABLE `word_battles`
  ADD CONSTRAINT `word_battles_ibfk_1` FOREIGN KEY (`id_ww`) REFERENCES `word_wars` (`id`);

--
-- Contraintes pour la table `ww_teams`
--
ALTER TABLE `ww_teams`
  ADD CONSTRAINT `ww_teams_ibfk_1` FOREIGN KEY (`id_ww`) REFERENCES `word_wars` (`id`);

--
-- Contraintes pour la table `ww_word_counts`
--
ALTER TABLE `ww_word_counts`
  ADD CONSTRAINT `ww_word_counts_ibfk_1` FOREIGN KEY (`id_ww`) REFERENCES `word_wars` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

